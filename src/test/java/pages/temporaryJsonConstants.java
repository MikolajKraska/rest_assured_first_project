package pages;

import java.util.List;

public class temporaryJsonConstants extends BasePage {

    public static String petName = "Piesoslaw Mariusz";
    public static int petId = 0;

    public static String secondNewPet = "Januszek";
    static List petList = getElement("/petclinic/api/pets").get("name");

    public static String mariola = "{\n" +
            "\"id\":0, \n" +
            "\"firstName\":\"Mariola\", \n" +
            "\"lastName\":\"Tadeusz\", \n" +
            "\"address\":\"Malpi Gaj 13c.\", \n" +
            "\"city\":\"Madison\", \n" +
            "\"telephone\":\"6085551023\" \n"+
            "}";

    public static String newPet =
            "\n" +
                    "{\n" +
                    "  \"birthDate\": \"2021/12/12\",\n" +
                    "  \"id\": "+petId+",\n" +
                    "  \"name\": \""+ petName +"\",\n" +
                    "  \"owner\": {\n" +
                    "   \t\t \"id\": 1,\n" +
                    "        \t\"firstName\": \"George\",\n" +
                    "        \t\"lastName\": \"Franklin\",\n" +
                    "        \t\"address\": \"110 W. Liberty St.\",\n" +
                    "        \t\"city\": \"Madison\",\n" +
                    "        \t\"telephone\": \"6085551023\"\n" +
                    "  },\n" +
                    "  \"type\": {\n" +
                    "\t\"id\": 2,\n" +
                    "\t\"name\": \"dog\"\n" +
                    "  }\n" +
                    "}\n";

    public static String anotherNewPet =
            "\n" +
                    "{\n" +
                    "  \"birthDate\": \"2021/12/12\",\n" +
                    "  \"id\": "+(petList.size()+1)+",\n" +
                    "  \"name\": \""+ secondNewPet +"\",\n" +
                    "  \"owner\": {\n" +
                    "   \t\t \"id\": 1,\n" +
                    "        \t\"firstName\": \"George\",\n" +
                    "        \t\"lastName\": \"Franklin\",\n" +
                    "        \t\"address\": \"110 W. Liberty St.\",\n" +
                    "        \t\"city\": \"Madison\",\n" +
                    "        \t\"telephone\": \"6085551023\"\n" +
                    "  },\n" +
                    "  \"type\": {\n" +
                    "\t\"id\": 2,\n" +
                    "\t\"name\": \"dog\"\n" +
                    "  }\n" +
                    "}\n";


}
