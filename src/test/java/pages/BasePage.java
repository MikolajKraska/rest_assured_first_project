package pages;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

import java.io.File;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;


public class BasePage {

    public String OWNER_ESTEBAN ="petclinic/api/owners/*/lastname/Estaban";
    public String OWNER_ESTABAN_ID ="petclinic/api/owners/10";
    public String OWNERS_LIST ="/petclinic/api/owners";
    public String OWNER_TADEUSZ = "petclinic/api/owners/*/lastname/Tadeusz";
    public String PETS_LIST = "/petclinic/api/pets";
    public String PET_TYPES_LIST = "petclinic/api/pets/pettypes";
    public String VISITS_LIST = "petclinic/api/visits";
    public String UPLOAD_PHOTO = "/petclinic/api/uploadPhoto/1";
    public String PHOTO_TOM = "/petclinic/api/downloadFile/1";


    public static JsonPath Estaban = new JsonPath(new File("C:\\Users\\MikolajKraska\\Desktop\\Java\\API\\rest_assured_first_project\\JsonFilesResources\\Estaban.json"));
    public static JsonPath Mateo = new JsonPath(new File("C:\\Users\\MikolajKraska\\Desktop\\Java\\API\\rest_assured_first_project\\JsonFilesResources\\Mateo.json"));
    public static JsonPath NewVisit = new JsonPath(new File("C:\\Users\\MikolajKraska\\Desktop\\Java\\API\\rest_assured_first_project\\JsonFilesResources\\NewVisit.json"));
    public static JsonPath NewEditedVisit = new JsonPath(new File("C:\\Users\\MikolajKraska\\Desktop\\Java\\API\\rest_assured_first_project\\JsonFilesResources\\NewEditedVisit.json"));
    public static File PhotoSmile = new File("C:\\Users\\MikolajKraska\\Desktop\\Java\\API\\rest_assured_first_project\\Images\\nowitam.png");


    public JsonPath printElement(String endPoint){
        RestAssured.baseURI= "http://localhost:9966";
        return given().when().get(endPoint)
                .then().log().all().assertThat().statusCode(200).extract().jsonPath();
    }

    public static JsonPath getElement(String endPoint){
        RestAssured.baseURI= "http://localhost:9966";
        return given().when().get(endPoint)
                .then().assertThat().statusCode(200).extract().jsonPath();
    }
    public void postNewElement(String endPoint, String jsonElement ){
        RestAssured.baseURI= "http://localhost:9966";
        given().contentType(ContentType.JSON)
                .body(jsonElement)
                .post(endPoint)
                .then().statusCode(201).extract().response();
    }
    public void assertIfElementNameIsCorrect(String endPoint, int size, String element) {
        RestAssured.baseURI= "http://localhost:9966";
        given().when().get(endPoint+"/"+ size)
                .then().log().all().assertThat().statusCode(200).body("name", equalTo(element));

    }
    public void compareWithExternalJsonFile(String endPoint, JsonPath expectedJson){
        RestAssured.baseURI= "http://localhost:9966";
        given().when().get(endPoint)
                .then().body("", equalTo(expectedJson.getMap("")));
    }
    public void putElementFromExternalJson(String endPoint, JsonPath expectedJson){
        RestAssured.baseURI= "http://localhost:9966";
        given().contentType(ContentType.JSON)
                .body(expectedJson.prettify())
                .put(endPoint)
                .then().statusCode(204).extract().response();
    }
    public static JsonPath deleteElement(String endPoint){
        RestAssured.baseURI= "http://localhost:9966";
        return given().when().delete(endPoint)
                .then().assertThat().statusCode(204).extract().jsonPath();
    }
    public Response uploadPhoto(File testUploadFile, String endPoint){
        RestAssured.baseURI= "http://localhost:9966";
        Response response = given().multiPart("photo", testUploadFile)
            .when().post(endPoint);

        System.out.println(response.getStatusCode());
        System.out.println(response.asString());
        return response;

    }
}
