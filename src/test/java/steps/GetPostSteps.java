package steps;


import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;

import io.restassured.response.Response;
import pages.BasePage;
import pages.temporaryJsonConstants;
import org.junit.Assert;

import java.util.List;

import static io.restassured.RestAssured.authentication;
import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;


public class GetPostSteps {
    BasePage basePage = new BasePage();

    private List petsAmount;
    private int size;

    @Given("GET information about owner")
    public void iPerformGETOperation() {
        JsonPath json = basePage.printElement(basePage.OWNER_ESTEBAN);

        System.out.println(json.getString("firstName"));
        System.out.println(json.getString("id"));
    }

    @And("POST new owner")
    public void iPerformPOST() {

        basePage.postNewElement(basePage.OWNERS_LIST, temporaryJsonConstants.mariola );

    }

    @Then("I should see the correct owner")
    public void iShouldSeeGET(){
        System.out.println(basePage.getElement(basePage.OWNER_TADEUSZ)
                .getString("firstName"));

    }

    @Given("List how many pets are there")
    public void listHowManyPetsAreThere() {
        petsAmount = basePage.getElement(basePage.PETS_LIST).get("id");

        System.out.println("There are " + petsAmount.size() +" pets in clinic");

        }

    @And("Get pet types and add another pet")
    public void getPetTypesAndAddAnother() {

        basePage.printElement(basePage.PET_TYPES_LIST);


        basePage.postNewElement(basePage.PETS_LIST, temporaryJsonConstants.newPet);

        List petList = basePage.getElement(basePage.PETS_LIST).get("id");

        basePage.assertIfElementNameIsCorrect(basePage.PETS_LIST,petList.size(),temporaryJsonConstants.petName);
    }

    @When("Change pet name")
    public void changePetName() {


        List petList = basePage.getElement(basePage.PETS_LIST).get("id");

        basePage.postNewElement(basePage.PETS_LIST, temporaryJsonConstants.anotherNewPet);


        given().when().get("petclinic/api/pets/"+ petList.size())
                .then().log().all().assertThat().statusCode(200).body("name", equalTo(temporaryJsonConstants.secondNewPet));

    }

    @Then("Check pets list size after adding another one")
    public void checkPetsList() {
        List petList = basePage.getElement(basePage.PETS_LIST).get("id");
        Assert.assertEquals(petsAmount.size(), petList.size() -1);
        System.out.println("Lista elemetów została poprawnie zwiększona z "+ petsAmount.size() + " do " + petList.size());

    }

    @Given("Try to add new doctor")
    public void tryToAddNewDoctor() {

        basePage.compareWithExternalJsonFile(basePage.OWNER_ESTABAN_ID, basePage.Estaban);

        basePage.postNewElement(basePage.OWNERS_LIST,basePage.Mateo.prettify());

        System.out.println(basePage.Mateo.prettify());

    }

    @And("New visit add and edit")
    public void newVisitAddAndEdit() {

        List visitsAmount = basePage.getElement(basePage.VISITS_LIST).get("id");

        basePage.postNewElement(basePage.VISITS_LIST,basePage.NewVisit.prettify());

        List visitsAmountAfterAddAnother = basePage.getElement(basePage.VISITS_LIST).get("id");

        Assert.assertEquals(visitsAmount.size(), visitsAmountAfterAddAnother.size() -1);

        basePage.putElementFromExternalJson(basePage.VISITS_LIST+"/"+ visitsAmountAfterAddAnother.size(), basePage.NewEditedVisit);
        System.out.println(basePage.VISITS_LIST+"/"+ visitsAmountAfterAddAnother.size());
        BasePage.deleteElement(basePage.VISITS_LIST+"/"+ visitsAmountAfterAddAnother.size());
        List finalQuantity = basePage.getElement(basePage.VISITS_LIST).get("id");
        System.out.println(finalQuantity.size());
    }

    @Given("Upload new image")
    public void uploadNewImage() {
        Response response = basePage.uploadPhoto(basePage.PhotoSmile.getAbsoluteFile(), basePage.UPLOAD_PHOTO);
        size = Integer.parseInt(response.jsonPath().get("size") + "");
    }

    @Then("Get newest image")
    public void getNewestImage() {

        System.out.println(basePage.getElement(basePage.PHOTO_TOM));

        RestAssured.baseURI= "http://localhost:9966";
        int photoSize = given().when().get("/petclinic/api/downloadFile/1")
                .then().assertThat().statusCode(200).extract().asByteArray().length;


        Assert.assertEquals((size), photoSize);






    }
}