Feature:
  Verif diferent GET operations using REST-assured


  Scenario: Verify one author
    Given GET information about owner
    And POST new owner
    Then I should see the correct owner

  Scenario: Verify pet list and add another
    Given List how many pets are there
    And Get pet types and add another pet
    When Change pet name
    Then Check pets list size after adding another one

  Scenario: Add new doctor and visit from json file
    Given Try to add new doctor
    And New visit add and edit

  Scenario: Upload Image
    Given Upload new image
    Then Get newest image


